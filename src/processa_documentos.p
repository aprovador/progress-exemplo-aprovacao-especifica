/**
 * Aprovador Sistemas LTDA - 2018
 * Acesse a documentacao da customizacao Progress do Aprovador em https://aprovador.com/documentacao. 
 *
 * FONTE LIVRE APROVADOR - Programa exemplo para aprovacao de documentos especificos.
 *
 * OBSERVACOES: Customize este fonte para atualizar o ERP com as aprovacoes/rejeicoes realizadas atraves do Aprovador.
 * - Este programa � executado apos o processamento padrao do Aprovador. 
 * - Se for uma integracao com o Totvs Datasul:
 *     + Apenas o banco emsfnd estara conectado quanto este programa for executado.
 *     + Utilize as procedures auxiliares connectCompany e disconnectCompany para conectar os bancos das empresas que precisar.
 * - Se for uma integracao com outros sitemas em Progress, todos os bancos informados estarao conectados.
 * - Dica: utilize este programa como ponto de entrada para as customizacoes e crie um programa para processamento de cada
 *         modulos (tabelas) especificos, para melhor organizacao, por exemplo:
 *     + src/processa_documentos_pagto.p
 *     + src/processa_documentos_venda.p
 *     + ...etc  
 * - A temp-table ttPendingDocument contera os documentos que foram aprovados pelo Aprovador e que precisam ser processados no ERP.
 * - A temp-table ttProcessedDocument devera ser alimentada com o resultado de cada aprovacao especifica processada. 
 */

{include/documents.i}
{include/aprovadorLib.i}
{include/fnd.i}

/* Parameters */
def input        parameter table for ttPendingDocument.
def input-output parameter table for ttProcessedDocument.

/* Locals */
def var cError      as char no-undo.
def var cCodEmpresa as char no-undo init "1".

/* Main block */

/**
 * Neste exemplo consideramos que este programa e' chamado por uma integracao com o Totvs Datasul, por isso
 * precisamos conectar nos bancos da Empresa.
 * - Caso seu ambiente possua mais de uma empresa com bancos diferentes, entao voce precisara conectar nos bancos 
 *   de cada empresa. Voce pode utilizar o campo ttPendingDocument.companyId, para saber em qual empresa conectar.
 */

run connectCompany(input  cCodEmpresa,
                   output cError).
if cError <> "" then do:
    run A_writeLog("Erro ao conectar na empresa " + cCodEmpresa + ": " + cError).
    return.
end.

run src/processa_documentos_esp.p(input        table ttPendingDocument,
                                  input-output table ttProcessedDocument).

run disconnectCompany(input cCodEmpresa).
